package com.contacts.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;

public abstract class ItemView {

    protected LinearLayout mLinearLayout;
    int layout = R.layout.contact_view_item;
    protected Context mContext;

    private LinearLayout inflate(Context context, int layout) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLinearLayout = (LinearLayout) inflater.inflate(layout , null);
        return mLinearLayout;
    }

    public abstract View getView(ContactDataItem param, Context context);

    protected View getView(Context context, int layout) {
        mContext = context;
        mLinearLayout = inflate(context, layout);
        initView();
        return mLinearLayout;
    }

	protected abstract void initView();

}
