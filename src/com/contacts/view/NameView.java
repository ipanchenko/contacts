package com.contacts.view;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.contacts.R;
import com.contacts.contact.model.Contact;
import com.contacts.contact.model.dataitem.ContactDataItem;

import com.contacts.contact.model.dataitem.PhotoItem;
import com.contacts.contact.model.dataitem.StructuredNameItem;

import java.util.ArrayList;
import java.util.HashMap;

public class NameView extends ItemView{

    private Contact mContact;
    private StructuredNameItem mName;
    private int layout = R.layout.contact_view_name;

    public View getView(Contact param, Context context) {
        mContact = param;
        return super.getView(context, layout);
    }

    @Override
    public View getView(ContactDataItem param, Context context) {
        return null;
    }

    protected void initView() {
        HashMap<String, ArrayList<ContentValues>> map = mContact.getValues().getMap();
        ContentValues cv = map.get(StructuredName.CONTENT_ITEM_TYPE).get(0);
        mName = new StructuredNameItem(cv);
        cv = map.get(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE).get(0);
        PhotoItem p = new PhotoItem(cv);
        TextView given_name = (TextView)mLinearLayout.findViewById(R.id.given_name);
        TextView family_name = (TextView)mLinearLayout.findViewById(R.id.family_name);
        TextView nickname = (TextView)mLinearLayout.findViewById(R.id.nickname);
        ImageView photo = (ImageView)mLinearLayout.findViewById(R.id.photo);
        //FIXME
        if(p.getPhoto() == null) {
            photo.setImageResource(android.R.drawable.stat_notify_sdcard_usb);
        } else {
            Bitmap bmp = BitmapFactory.decodeByteArray(p.getPhoto(), 0, p.getPhoto().length);
            photo.setImageBitmap(bmp);
        }
        given_name.setText(mName.getGivenName());
        family_name.setText(mName.getFamilyName());
//        String nick = modelItem.rawContacts.get(Nickname.CONTENT_ITEM_TYPE).get(0).DATA.get(0);
//        nickname.setText(nick);
    }
}