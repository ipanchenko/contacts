package com.contacts.view.edit;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.*;
import com.contacts.ContactsApp;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.StructuredPostalItem;
import com.contacts.view.ItemView;

public class PostalView extends ItemView {

    private int layout = R.layout.contact_edit_postal_edit;
    private StructuredPostalItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new StructuredPostalItem(param.getContentValues());
        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        Spinner post_type = (Spinner)mLinearLayout.findViewById(R.id.post_type);
        EditText street_edit = (EditText)mLinearLayout.findViewById(R.id.street_edit);
        EditText pobox_edit = (EditText)mLinearLayout.findViewById(R.id.pobox_edit);
        EditText neighborhood_edit = (EditText)mLinearLayout.findViewById(R.id.neighborhood_edit);
        EditText city_edit = (EditText)mLinearLayout.findViewById(R.id.city_edit);
        EditText region_edit = (EditText)mLinearLayout.findViewById(R.id.region_edit);
        EditText postcode_edit = (EditText)mLinearLayout.findViewById(R.id.postcode_edit);
        EditText country_edit = (EditText)mLinearLayout.findViewById(R.id.country_edit);

//        modelItem.DATA.set(1, String.valueOf(post_type.getSelectedItemPosition()));
//        modelItem.DATA.set(3, street_edit.getText().toString());
//        modelItem.DATA.set(4, pobox_edit.getText().toString());
//        modelItem.DATA.set(5, neighborhood_edit.getText().toString());
//        modelItem.DATA.set(6, city_edit.getText().toString());
//        modelItem.DATA.set(7, region_edit.getText().toString());
//        modelItem.DATA.set(8, postcode_edit.getText().toString());
//        modelItem.DATA.set(9, country_edit.getText().toString());

        return modelItem;
    }

    @Override
    protected void initView() {
        Spinner post_type = (Spinner)mLinearLayout.findViewById(R.id.post_type);
        EditText street_edit = (EditText)mLinearLayout.findViewById(R.id.street_edit);
        EditText pobox_edit = (EditText)mLinearLayout.findViewById(R.id.pobox_edit);
        EditText neighborhood_edit = (EditText)mLinearLayout.findViewById(R.id.neighborhood_edit);
        EditText city_edit = (EditText)mLinearLayout.findViewById(R.id.city_edit);
        EditText region_edit = (EditText)mLinearLayout.findViewById(R.id.region_edit);
        EditText postcode_edit = (EditText)mLinearLayout.findViewById(R.id.postcode_edit);
        EditText country_edit = (EditText)mLinearLayout.findViewById(R.id.country_edit);

        street_edit.setText(modelItem.getStreet(), TextView.BufferType.EDITABLE);
        pobox_edit.setText(modelItem.getPOBox(), TextView.BufferType.EDITABLE);
        neighborhood_edit.setText(modelItem.getNeighborhood(), TextView.BufferType.EDITABLE);
        city_edit.setText(modelItem.getCity(), TextView.BufferType.EDITABLE);
        region_edit.setText(modelItem.getRegion(), TextView.BufferType.EDITABLE);
        postcode_edit.setText(modelItem.getPostcode(), TextView.BufferType.EDITABLE);
        country_edit.setText(modelItem.getCountry(), TextView.BufferType.EDITABLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
                ContactsApp.sTypeNames.get(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        post_type.setAdapter(adapter);
        post_type.setPrompt("Select type");
        post_type.setSelection(Integer.valueOf(modelItem.getType()));

        ImageView delete = (ImageView)mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }

}