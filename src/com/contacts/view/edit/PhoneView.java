package com.contacts.view.edit;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.*;
import com.contacts.ContactsApp;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.PhoneItem;
import com.contacts.view.ItemView;

public class PhoneView extends ItemView {

    private int layout = R.layout.contact_edit_phone_edit;
    private PhoneItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new PhoneItem(param.getContentValues());

        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        Spinner phone_type = (Spinner)mLinearLayout.findViewById(R.id.phone_type);
        EditText phone_edit = (EditText)mLinearLayout.findViewById(R.id.phone_edit);
//        modelItem.DATA.set(0, phone_edit.getText().toString());
//        modelItem.DATA.set(1, String.valueOf(phone_type.getSelectedItemPosition()));

        return modelItem;
    }

    @Override
    protected void initView() {
        Spinner phone_type = (Spinner)mLinearLayout.findViewById(R.id.phone_type);
        EditText phone_edit = (EditText)mLinearLayout.findViewById(R.id.phone_edit);

        phone_edit.setText(modelItem.getNumber(), TextView.BufferType.EDITABLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
                ContactsApp.sTypeNames.get(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        phone_type.setAdapter(adapter);
        phone_type.setPrompt("Select type");
        phone_type.setSelection(Integer.valueOf(modelItem.getType()));

        ImageView delete = (ImageView)mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }
}