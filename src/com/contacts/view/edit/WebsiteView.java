package com.contacts.view.edit;

import android.content.Context;
import android.view.View;
import android.widget.*;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.WebsiteItem;
import com.contacts.view.ItemView;

public class WebsiteView extends ItemView {

    private int layout = R.layout.contact_edit_url_edit;
    private WebsiteItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new WebsiteItem(param.getContentValues());
        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        EditText web_url = (EditText)mLinearLayout.findViewById(R.id.web_edit);
//        modelItem.DATA.set(0, web_url.getText().toString());

        return modelItem;
    }

    @Override
    protected void initView() {
        EditText web_url = (EditText)mLinearLayout.findViewById(R.id.web_edit);
        web_url.setText(modelItem.getUrl());

//        Spinner web_type = (Spinner)mLinearLayout.findViewById(R.id.web_type);
//        ArrayAdapter<String> type_adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, ContactsApp.Website_Type);
//        type_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        web_type.setAdapter(type_adapter);
//        web_type.setPrompt("Select type");
//        web_type.setSelection(Integer.valueOf(modelItem.getType()));

        ImageView delete = (ImageView)mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }
}