package com.contacts.view.edit;

import android.content.ContentValues;
import android.content.Context;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.contacts.R;
import com.contacts.contact.model.Contact;
import com.contacts.contact.model.dataitem.StructuredNameItem;

public class NameView{

    private int layout = R.layout.contact_edit_name_edit;
    private Contact mContact;
    private LinearLayout mLinearLayout;
    private Context mContext;
    private StructuredNameItem mName;

    public View getView(Contact param, Context context) {
        mContact = param;
        mContext = context;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLinearLayout = (LinearLayout) inflater.inflate(layout , null);
        initView();

        return mLinearLayout;
    }

    public void getValue(Contact contact) {
        EditText given_name = (EditText)mLinearLayout.findViewById(R.id.given_name);
        EditText family_name = (EditText)mLinearLayout.findViewById(R.id.family_name);
        EditText middle_name = (EditText)mLinearLayout.findViewById(R.id.middle_name);
        EditText suffix_name = (EditText)mLinearLayout.findViewById(R.id.suffix_name);
        EditText prefix_name = (EditText)mLinearLayout.findViewById(R.id.prefix_name);
        EditText phonetic_given_name = (EditText)mLinearLayout.findViewById(R.id.mphonetic_given_name);
        EditText phonetic_family_name = (EditText)mLinearLayout.findViewById(R.id.mphonetic_family_name);

//        contact.rawContacts.get().set(1, given_name.getText().toString());
//        contact.Name.DATA.set(2, family_name.getText().toString());
//        contact.Name.DATA.set(3, middle_name.getText().toString());
//        contact.Name.DATA.set(4, suffix_name.getText().toString());
//        contact.Name.DATA.set(5, prefix_name.getText().toString());
//        contact.Name.DATA.set(6, phonetic_given_name.getText().toString());
//        contact.Name.DATA.set(7, phonetic_family_name.getText().toString());
    }

    private void initView() {
        ContentValues cv = mContact.getValues().getMap().get(StructuredName.CONTENT_ITEM_TYPE).get(0);
        mName = new StructuredNameItem(cv);
        ImageView photo = (ImageView)mLinearLayout.findViewById(R.id.photo_add);
        EditText given_name = (EditText)mLinearLayout.findViewById(R.id.given_name);
        EditText family_name = (EditText)mLinearLayout.findViewById(R.id.family_name);
        EditText middle_name = (EditText)mLinearLayout.findViewById(R.id.middle_name);
        EditText suffix_name = (EditText)mLinearLayout.findViewById(R.id.suffix_name);
        EditText prefix_name = (EditText)mLinearLayout.findViewById(R.id.prefix_name);
        EditText phonetic_given_name = (EditText)mLinearLayout.findViewById(R.id.mphonetic_given_name);
        EditText phonetic_family_name = (EditText)mLinearLayout.findViewById(R.id.mphonetic_family_name);
        //FIXME
        photo.setImageResource(android.R.drawable.ic_menu_info_details);
        given_name.setText(mName.getGivenName(), TextView.BufferType.EDITABLE);
        family_name.setText(mName.getFamilyName(), TextView.BufferType.EDITABLE);
        middle_name.setText(mName.getMiddleName(), TextView.BufferType.EDITABLE);
        suffix_name.setText(mName.getSuffix(), TextView.BufferType.EDITABLE);
        prefix_name.setText(mName.getPrefix(), TextView.BufferType.EDITABLE);
        phonetic_family_name.setText(mName.getPhoneticFamilyName(), TextView.BufferType.EDITABLE);
        phonetic_given_name.setText(mName.getPhoneticGivenName(), TextView.BufferType.EDITABLE);
    }

}