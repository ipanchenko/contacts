package com.contacts.view.edit;

import android.content.Context;
import android.view.View;
import android.widget.*;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.NoteItem;
import com.contacts.view.ItemView;

public class NoteView extends ItemView {

    private int layout = R.layout.contact_edit_note_edit;
    private NoteItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new NoteItem(param.getContentValues());
        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        EditText note = (EditText)mLinearLayout.findViewById(R.id.note_edit);
//        modelItem.DATA.set(0, note.getText().toString());

        return modelItem;
    }

    @Override
    protected void initView() {
        EditText note = (EditText)mLinearLayout.findViewById(R.id.note_edit);
        note.setText(modelItem.getNote());

        ImageView delete = (ImageView)mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }

}