package com.contacts.view.edit;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.*;
import com.contacts.ContactsApp;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.EmailItem;
import com.contacts.view.ItemView;

public class EmailView extends ItemView {

    private int layout = R.layout.contact_edit_email_edit;
    private EmailItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new EmailItem(param.getContentValues());
        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
        Spinner email_type = (Spinner)mLinearLayout.findViewById(R.id.email_type);
        EditText email_edit = (EditText)mLinearLayout.findViewById(R.id.email_edit);
//        modelItem.DATA.set(1, String.valueOf(email_type.getSelectedItemPosition()));
//        modelItem.DATA.set(0, email_edit.getText().toString());

        return modelItem;
    }

    @Override
    protected void initView() {
        Spinner email_type = (Spinner)mLinearLayout.findViewById(R.id.email_type);
        EditText email_edit = (EditText)mLinearLayout.findViewById(R.id.email_edit);
        email_edit.setText(modelItem.getAddress(), TextView.BufferType.EDITABLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
                ContactsApp.sTypeNames.get(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        email_type.setAdapter(adapter);
        email_type.setPrompt("Select type");
        email_type.setSelection(Integer.valueOf(modelItem.getType()));

        ImageView delete = (ImageView)mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }
}