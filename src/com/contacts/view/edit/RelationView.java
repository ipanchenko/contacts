package com.contacts.view.edit;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.*;
import com.contacts.ContactsApp;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.RelationItem;
import com.contacts.view.ItemView;

public class RelationView extends ItemView {

    private int layout = R.layout.contact_edit_relation_edit;
    private RelationItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new RelationItem(param.getContentValues());
        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        Spinner rel_type = (Spinner)mLinearLayout.findViewById(R.id.rel_type);
        EditText rel_name = (EditText)mLinearLayout.findViewById(R.id.rel_edit);
//        modelItem.DATA.set(0, rel_name.getText().toString());
//        modelItem.DATA.set(1, String.valueOf(rel_type.getSelectedItemPosition()));

        return modelItem;
    }

    @Override
    protected void initView() {
        //FIXME
        Spinner rel_type = (Spinner)mLinearLayout.findViewById(R.id.rel_type);
        EditText rel_name = (EditText)mLinearLayout.findViewById(R.id.rel_edit);
        rel_name.setText(modelItem.getName(), TextView.BufferType.EDITABLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
                ContactsApp.sTypeNames.get(ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rel_type.setAdapter(adapter);
        rel_type.setPrompt("Select type");
        rel_type.setSelection(Integer.valueOf(modelItem.getType()));

        ImageView delete = (ImageView)mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }
}