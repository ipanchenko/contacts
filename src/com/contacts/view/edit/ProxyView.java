package com.contacts.view.edit;

import android.provider.ContactsContract;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.view.ItemView;

public class ProxyView {
    public static ItemView getViewClass(ContactDataItem item) {
        return getViewClass(item.getMimeType());
    }

    public static ItemView getViewClass(String mime) {
        if(mime.compareTo(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE) == 0)
            return new EmailView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE) == 0)
            return new EventView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE) == 0)
            return new ImView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE) == 0)
            return new NicknameView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE) == 0)
            return new NoteView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE) == 0)
            return new OrganizationView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE) == 0)
            return new PhoneView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE) == 0)
            return new PostalView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE) == 0)
            return new RelationView();

        if(mime.compareTo(ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE) == 0)
            return new WebsiteView();

        //FIXME return some view for unknown mimetypes
         return null;
    }

    public static String getMimeName(String mime) {
        //FIXME i18n
        if(mime.compareTo(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE) == 0)
            return "Email";

        if(mime.compareTo(ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE) == 0)
            return "Event";

        if(mime.compareTo(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE) == 0)
            return "Im";

        if(mime.compareTo(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE) == 0)
            return "Nickname";

        if(mime.compareTo(ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE) == 0)
            return "Note";

        if(mime.compareTo(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE) == 0)
            return "Organization";

        if(mime.compareTo(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE) == 0)
            return "Phone";

        if(mime.compareTo(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE) == 0)
            return "Postal";

        if(mime.compareTo(ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE) == 0)
            return "Relation";

        if(mime.compareTo(ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE) == 0)
            return "Website";

        return "Unknown";
    }
}
