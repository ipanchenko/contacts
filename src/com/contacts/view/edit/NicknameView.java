package com.contacts.view.edit;

import android.content.Context;
import android.view.View;
import android.widget.*;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.NicknameItem;
import com.contacts.view.ItemView;

public class NicknameView extends ItemView {

    private int layout = R.layout.contact_edit_nickname_edit;
    private NicknameItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new NicknameItem(param.getContentValues());

        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        EditText nick_edit = (EditText) mLinearLayout.findViewById(R.id.nick_edit);
//        modelItem.DATA.set(0, nick_edit.getText().toString());

        return modelItem;
    }

    @Override
    protected void initView() {
        EditText nick_edit = (EditText) mLinearLayout.findViewById(R.id.nick_edit);
        nick_edit.setText(modelItem.getName(), TextView.BufferType.EDITABLE);

        ImageView delete = (ImageView) mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }

}