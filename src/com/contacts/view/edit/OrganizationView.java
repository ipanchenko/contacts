package com.contacts.view.edit;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.*;
import com.contacts.ContactsApp;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.OrganizationItem;
import com.contacts.view.ItemView;

public class OrganizationView extends ItemView {

    private int layout = R.layout.contact_edit_org_edit;
    private OrganizationItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new OrganizationItem(param.getContentValues());
        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        EditText company_edit = (EditText) mLinearLayout.findViewById(R.id.company_edit);
        EditText title = (EditText) mLinearLayout.findViewById(R.id.title_edit);
        EditText department_edit = (EditText) mLinearLayout.findViewById(R.id.department_edit);
        Spinner type = (Spinner) mLinearLayout.findViewById(R.id.org_type);
//        modelItem.DATA.set(0, company_edit.getText().toString());
//        modelItem.DATA.set(1, String.valueOf(type.getSelectedItemPosition()));
//        modelItem.DATA.set(3, title.getText().toString());
//        modelItem.DATA.set(4, department_edit.getText().toString());

        return modelItem;
    }

    @Override
    protected void initView() {
        EditText company_edit = (EditText) mLinearLayout.findViewById(R.id.company_edit);
        EditText title = (EditText) mLinearLayout.findViewById(R.id.title_edit);
        EditText department_edit = (EditText) mLinearLayout.findViewById(R.id.department_edit);
        Spinner type = (Spinner) mLinearLayout.findViewById(R.id.org_type);

        company_edit.setText(modelItem.getCompany(), TextView.BufferType.EDITABLE);
        title.setText(modelItem.getTitle(), TextView.BufferType.EDITABLE);
        department_edit.setText(modelItem.getDepartment(), TextView.BufferType.EDITABLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
                ContactsApp.sTypeNames.get(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(adapter);
        type.setPrompt("Select type");
        type.setSelection(Integer.valueOf(modelItem.getType()));

        ImageView delete = (ImageView) mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }
}