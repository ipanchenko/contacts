package com.contacts.view.edit;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.*;
import com.contacts.ContactsApp;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.ImItem;
import com.contacts.view.ItemView;

public class ImView extends ItemView {

    private int layout = R.layout.contact_edit_im_edit;
    private ImItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new ImItem(param.getContentValues());
        return super.getView(context, layout);
    }

	public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        Spinner im_type = (Spinner) mLinearLayout.findViewById(R.id.im_type);
        Spinner im_prot = (Spinner) mLinearLayout.findViewById(R.id.im_prot);
        EditText im_edit = (EditText) mLinearLayout.findViewById(R.id.im_edit);

//        modelItem.DATA.set(0, im_edit.getText().toString());
//        modelItem.DATA.set(1, String.valueOf(im_prot.getSelectedItemPosition()));
//        modelItem.DATA.set(4, String.valueOf(im_type.getSelectedItemPosition()-1));

		return modelItem;
	}

	@Override
	protected void initView() {
        Spinner im_type = (Spinner) mLinearLayout.findViewById(R.id.im_type);
        Spinner im_prot = (Spinner) mLinearLayout.findViewById(R.id.im_prot);
        EditText im_edit = (EditText) mLinearLayout.findViewById(R.id.im_edit);
        im_edit.setText(modelItem.getData(), TextView.BufferType.EDITABLE);

        ArrayAdapter<String> type_adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item,
                ContactsApp.sTypeNames.get(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE));
        type_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        im_type.setAdapter(type_adapter);
        im_type.setPrompt("Select type");
        im_type.setSelection(Integer.valueOf(modelItem.getType()));


        ArrayAdapter<String> prot_adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, ContactsApp.Im_Prot);
        prot_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        im_prot.setAdapter(prot_adapter);
        im_prot.setPrompt("Select protocol");
        im_prot.setSelection(modelItem.getProtocol() + 1);

        ImageView delete = (ImageView)mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
	}
}