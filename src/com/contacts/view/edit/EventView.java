package com.contacts.view.edit;

import android.app.DatePickerDialog;
import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import com.contacts.R;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.model.dataitem.EventItem;
import com.contacts.view.ItemView;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class EventView extends ItemView {

    private int layout = R.layout.contact_edit_event_edit;
    private EventItem modelItem;
    private TextView mCurrentEvent;
    private Context mContext;

    public View getView(ContactDataItem item, Context context) {
        mContext = context;
        modelItem = new EventItem(item.getContentValues());
        return super.getView(context, layout);
    }

    public ContactDataItem getValue() {
//        if(modelItem.state == ContactDataItem.STATE.gone)
//            return null;

        TextView event_edit = (TextView)mLinearLayout.findViewById(R.id.event_edit);
//        modelItem.DATA.set(0, event_edit.getText().toString());

        return modelItem;
    }

    @Override
    protected void initView() {
        TextView event_label = (TextView)mLinearLayout.findViewById(R.id.event_type);
        TextView event_date = (TextView)mLinearLayout.findViewById(R.id.event_edit);
        event_label.setText(mContext.getString(ContactsContract.CommonDataKinds.Event.getTypeResource(Integer.valueOf(modelItem.getType()))));
        event_date.setText(modelItem.getStartDate());
        mLinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mCurrentEvent = (TextView) v.findViewById(R.id.event_edit);
                String dateString = mCurrentEvent.getText().toString();
                Date date = new Date();

                try {
                    date = DateFormat.getDateInstance().parse(dateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                DatePickerDialog tpd = new DatePickerDialog(mContext, myCallBack, 1900 + date.getYear(), date.getMonth(), date.getDate());
                tpd.show();
            }
        });

        ImageView delete = (ImageView)mLinearLayout.findViewById(R.id.field_del);
        delete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearLayout.setVisibility(View.GONE);
            }
        });
    }

    private DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Date date = new Date();
            date.setDate(dayOfMonth);
            date.setMonth(monthOfYear);
            date.setYear(year-1900);
            String dateString = DateFormat.getDateInstance().format(date);

            if(mCurrentEvent != null) {
                mCurrentEvent.setText(dateString);
                mCurrentEvent = null;
            }
        }
    };
}