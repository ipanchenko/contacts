package com.contacts.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.contacts.R;
import com.contacts.contact.model.ActionItem;
import com.contacts.contact.model.dataitem.ContactDataItem;

public class ContactDataItemView extends ItemView {

    private ActionItem modelItem;

    public View getView(ContactDataItem param, Context context) {
        modelItem = new ActionItem(param);
        return super.getView(context, layout);
    }

    @Override
    protected void initView() {
        TextView title = (TextView)mLinearLayout.findViewById(R.id.title);
        TextView content = (TextView)mLinearLayout.findViewById(R.id.content);
        ImageView icon = (ImageView)mLinearLayout.findViewById(R.id.icon_btn);

        title.setText(modelItem.getTitle());
        content.setText(modelItem.getContent());
        if(modelItem.hasAction()) {
            icon.setVisibility(View.VISIBLE);
            icon.setImageResource(modelItem.getActionIcon());
            icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(modelItem.getAction().resolveActivity(mContext.getPackageManager()) != null) {
                        mContext.startActivity(modelItem.getAction());
                    }
                }
            });
            icon.setTag(modelItem.getAction());
        }
        if(modelItem.hasAltAction()) {
            //FIXME
        }
    }
}