package com.contacts.view;

import android.provider.ContactsContract.CommonDataKinds.*;
import com.contacts.contact.model.dataitem.ContactDataItem;

public class ProxyView {
    public static ItemView getViewClass(ContactDataItem item) {
        return getViewClass(item.getMimeType());
    }

    public static ItemView getViewClass(String mime) {
        if((mime.compareTo(Email.CONTENT_ITEM_TYPE) == 0) ||
        (mime.compareTo(Event.CONTENT_ITEM_TYPE) == 0) ||
        (mime.compareTo(Im.CONTENT_ITEM_TYPE) == 0) ||
        (mime.compareTo(Note.CONTENT_ITEM_TYPE) == 0) ||
        (mime.compareTo(Organization.CONTENT_ITEM_TYPE) == 0) ||
        (mime.compareTo(Phone.CONTENT_ITEM_TYPE) == 0) ||
        (mime.compareTo(StructuredPostal.CONTENT_ITEM_TYPE) == 0) ||
        (mime.compareTo(Relation.CONTENT_ITEM_TYPE) == 0) ||
        (mime.compareTo(Website.CONTENT_ITEM_TYPE) == 0))
            //FIXME return some view for unknown mimetypes
            return new ContactDataItemView();

        return null;
    }

}
