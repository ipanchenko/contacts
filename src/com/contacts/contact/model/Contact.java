package com.contacts.contact.model;

import android.net.Uri;

import java.util.ArrayList;

public class Contact{

    private final String mDirectoryParameter;
    private final long mDirectoryId;
    private final long mContactId;
    private final String mLookupKey;
    private final int mDisplayNameSource;
    private final String mDisplayName;
    private final String mAltDisplayName;
    private final String mPhoneticName;
    private final long mPhotoId;
    private final String mPhotoUri;
    private final boolean isStarred;
    private final Integer mPresence;
    private final boolean isSendToVoicemail;
    private final String mCustomRingtone;
    private final boolean isUserProfile;
    private final Uri mLookupUri;

    private ArrayList<RawContact> rawContacts = new  ArrayList<RawContact>();

    public Contact(String directoryParameter, long directoryId, long contactId,
                   String lookupKey, int displayNameSource, String displayName,
                   String altDisplayName, String phoneticName, long photoId,
                   String photoUri, boolean starred, Integer presence,
                   boolean sendToVoicemail, String customRingtone,
                   boolean isProfile, Uri lookupUri) {
        mDirectoryParameter = directoryParameter;
        mDirectoryId = directoryId;
        mContactId = contactId;
        mLookupKey = lookupKey;
        mDisplayNameSource = displayNameSource;
        mDisplayName = displayName;
        mAltDisplayName = altDisplayName;
        mPhoneticName = phoneticName;
        mPhotoId = photoId;
        mPhotoUri = photoUri;
        isStarred = starred;
        mPresence = presence;
        isSendToVoicemail = sendToVoicemail;
        mCustomRingtone = customRingtone;
        isUserProfile = isProfile;
        mLookupUri = lookupUri;
    }

    public String getDirectoryParameter() {
        return mDirectoryParameter;
    }

    public long getDirectoryId() {
        return mDirectoryId;
    }

    public long getContactId() {
        return mContactId;
    }

    public String getLookupKey() {
        return mLookupKey;
    }

    public int getDisplayNameSource() {
        return mDisplayNameSource;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public String getAltDisplayName() {
        return mAltDisplayName;
    }

    public String getPhoneticName() {
        return mPhoneticName;
    }

    public long getPhotoId() {
        return mPhotoId;
    }

    public String getPhotoUri() {
        return mPhotoUri;
    }

    public boolean isStarred() {
        return isStarred;
    }

    public Integer getPresence() {
        return mPresence;
    }

    public boolean isSendToVoicemail() {
        return isSendToVoicemail;
    }

    public String getCustomRingtone() {
        return mCustomRingtone;
    }

    public boolean isUserProfile() {
        return isUserProfile;
    }

    public Uri getLookupUri() {
        return mLookupUri;
    }

    public ArrayList<RawContact> getRawContacts() {
        return rawContacts;
    }

    public void setRawContacts (ArrayList<RawContact> list) {
        rawContacts.addAll(list);
    }

    public RawContact getValues() {
        if(rawContacts.size() == 1)
            return rawContacts.get(0);

        return null;
    }

    public int hashCode() {
        int hash = 0;
        hash += this.rawContacts.hashCode();
        return hash;
    }
}
