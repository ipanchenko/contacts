package com.contacts.contact.model;

import android.content.ContentValues;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.HashMap;

public class RawContact {

    private ContentValues mContentValues;

    private ArrayList<ContentValues> mDataItems;

    public RawContact(ContentValues values) {
        mContentValues = values;
        mDataItems = new ArrayList<ContentValues>();
    }

    public void addDataItemValues(ContentValues data) {
        mDataItems.add(data);
    }

    public ArrayList<ContentValues> getDataItems() {
        return mDataItems;
    }

    public HashMap<String, ArrayList<ContentValues>> getMap() {
        HashMap<String, ArrayList<ContentValues>> map = new HashMap<String, ArrayList<ContentValues>>();
        for(ContentValues item : mDataItems) {
            String mime = item.getAsString(ContactsContract.Data.MIMETYPE);
            if(!map.containsKey(mime))
                map.put(mime, new ArrayList<ContentValues>());
            map.get(mime).add(item);
        }
        return map;
    }
}
