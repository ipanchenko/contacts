package com.contacts.contact.model;

import android.content.Intent;
import android.net.Uri;
import com.contacts.contact.model.dataitem.*;

public class ActionItem {

    private String mTitle;
    private String mContent;
    private boolean hasAction = false;
    private boolean hasAltAction = false;
    private int actionIcon;
    private int altActionIcon;
    private Intent action;
    private Intent altAction;

    public ActionItem(ContactDataItem item) {
        mTitle = item.getTitleForDisplay();
        mContent = item.getContentForDisplay();

        if(item instanceof EmailItem) {
            hasAction = true;
            actionIcon = android.R.drawable.sym_action_email;
            action = new Intent(Intent.ACTION_SENDTO, Uri.parse("emailto:"+((EmailItem) item).getData()));
        }

        if(item instanceof EventItem) {
            //FIXME open calendar
            actionIcon = android.R.drawable.sym_contact_card;
        }

        if(item instanceof ImItem) {
            actionIcon = android.R.drawable.sym_action_chat;
        }

        if(item instanceof StructuredPostalItem) {
            StructuredPostalItem spi = (StructuredPostalItem)item;
            hasAction = true;
            actionIcon = android.R.drawable.ic_dialog_map;
            action = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + Uri.encode(spi.getFormattedAddress())));
        }

        if(item instanceof OrganizationItem) {
            OrganizationItem spi = (OrganizationItem)item;
            hasAction = true;
            actionIcon = android.R.drawable.ic_dialog_map;
            action = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + Uri.encode(spi.getContentForDisplay())));
        }

        if(item instanceof PhoneItem) {
            PhoneItem phone = (PhoneItem) item;
            hasAction = true;
            hasAltAction = true;
            actionIcon = android.R.drawable.sym_action_call;
            altActionIcon = android.R.drawable.sym_action_chat;
            action = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phone.getNumber()));
            altAction = new Intent(Intent.ACTION_SENDTO, Uri.parse(phone.getNumber()));
        }
    }

    public String getTitle() {
        return mTitle;
    }

    public String getContent() {
        return mContent;
    }

    public boolean hasAction() {
        return hasAction;
    }

    public boolean hasAltAction() {
        return hasAltAction;
    }

    public int getActionIcon() {
        return actionIcon;
    }

    public int getAltActionIcon() {
        return altActionIcon;
    }

    public Intent getAction() {
        return action;
    }

    public Intent getAltAction() {
        return altAction;
    }

}
