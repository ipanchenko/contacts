package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract;
import com.google.common.base.Objects;

public class ContactDataItem {

    private final ContentValues mValues;

    public ContactDataItem(ContentValues values) {
        mValues = values;
    }

    public static ContactDataItem createFrom(ContentValues values) {
        final String mimeType = values.getAsString(ContactsContract.Contacts.Data.MIMETYPE);
        if (ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new StructuredNameItem(values);
        } else if (ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new PhoneItem(values);
        } else if (ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new EmailItem(values);
        } else if (ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new StructuredPostalItem(values);
        } else if (ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new ImItem(values);
        } else if (ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new OrganizationItem(values);
        } else if (ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new NicknameItem(values);
        } else if (ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new NoteItem(values);
        } else if (ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new WebsiteItem(values);
        } else if (ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new EventItem(values);
        } else if (ContactsContract.CommonDataKinds.Relation.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new RelationItem(values);
        } else if (ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE.equals(mimeType)) {
            return new PhotoItem(values);
        }

        return new ContactDataItem(values);
    }

    @Override
    public boolean equals(Object obj) {
        return Objects.equal(this, obj);
    }

    public ContentValues getContentValues() {
        return mValues;
    }

    public String getMimeType() {
        return mValues.getAsString(ContactsContract.Contacts.Data.MIMETYPE);
    }

    public String getTitleForDisplay() {
        return getMimeType();
    }

    public String getContentForDisplay() {
        return mValues.toString();
    }
}
