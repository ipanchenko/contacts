package com.contacts.contact.model.dataitem;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.os.Build;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import com.contacts.ContactsApp;

public class PhoneItem extends ContactDataItem {

    public PhoneItem(ContentValues values) {
        super(values);
    }

    public String getNumber() {
        return getContentValues().getAsString(Phone.NUMBER);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public String getNormalizedNumber() {
        return getContentValues().getAsString(Phone.NORMALIZED_NUMBER);
    }

    public int getType() {
        return getContentValues().getAsInteger(Phone.TYPE);
    }

    public String getLabel() {
        return getContentValues().getAsString(Phone.LABEL);
    }

    public String getTitleForDisplay() {
        return getNumber();
    }

    public String getContentForDisplay() {
        return ContactsApp.sTypeNames.get(getMimeType())[getType()];
    }
}
