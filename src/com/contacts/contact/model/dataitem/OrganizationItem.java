package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.text.TextUtils;
import com.contacts.ContactsApp;

public class OrganizationItem extends ContactDataItem {

    public OrganizationItem(ContentValues values) {
        super(values);
    }

    public String getCompany() {
        return getContentValues().getAsString(Organization.COMPANY);
    }

    public int getType() {
        return getContentValues().getAsInteger(Organization.TYPE);
    }

    public String getLabel() {
        return getContentValues().getAsString(Organization.LABEL);
    }

    public String getTitle() {
        return getContentValues().getAsString(Organization.TITLE);
    }

    public String getDepartment() {
        return getContentValues().getAsString(Organization.DEPARTMENT);
    }

    private String getJobDescription() {
        return getContentValues().getAsString(Organization.JOB_DESCRIPTION);
    }

    public String getSymbol() {
        return getContentValues().getAsString(Organization.SYMBOL);
    }

    public String getPhoneticName() {
        return getContentValues().getAsString(Organization.PHONETIC_NAME);
    }

    public String getOfficeLocation() {
        return getContentValues().getAsString(Organization.OFFICE_LOCATION);
    }

    public String getPhoneticNameStyle() {
        return getContentValues().getAsString(Organization.PHONETIC_NAME_STYLE);
    }

    public String getTitleForDisplay() {
        return ContactsApp.sTypeNames.get(getMimeType())[getType()];
    }

    public String getContentForDisplay() {
        StringBuilder sb = new StringBuilder();
        addLine(sb, getTitle());
        addLine(sb, getDepartment());
        addLine(sb, getJobDescription());
        return sb.toString();
    }

    private StringBuilder addLine(StringBuilder sb, String line) {
        if(!TextUtils.isEmpty(line)) {
            sb.append(line);
            sb.append("\n");
        }
        return sb;
    }
}
