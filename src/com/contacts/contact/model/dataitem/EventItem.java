package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.Event;
import com.contacts.ContactsApp;

public class EventItem extends ContactDataItem {

    public EventItem(ContentValues values) {
        super(values);
    }

    public String getStartDate() {
        return getContentValues().getAsString(Event.START_DATE);
    }

    public int getType() {
        return getContentValues().getAsInteger(Event.TYPE);
    }

    public String getLabel() {
        return getContentValues().getAsString(Event.LABEL);
    }

    public String getTitleForDisplay() {
        return getStartDate();
    }

    public String getContentForDisplay() {
        return ContactsApp.sTypeNames.get(getMimeType())[getType()];
    }
}
