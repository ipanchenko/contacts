package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.Im;
import com.contacts.ContactsApp;

public class ImItem extends ContactDataItem{

    public ImItem(ContentValues values) {
        super(values);
    }

    public String getData() {
        return getContentValues().getAsString(Im.DATA);
    }

    public int getType() {
        return getContentValues().getAsInteger(Im.TYPE);
    }

    public String getLabel() {
        return getContentValues().getAsString(Im.LABEL);
    }

    public Integer getProtocol() {
        return getContentValues().getAsInteger(Im.PROTOCOL);
    }

    public boolean isProtocolValid() {
        return getProtocol() != null;
    }

    public String getCustomProtocol() {
        return getContentValues().getAsString(Im.CUSTOM_PROTOCOL);
    }

    public int getChatCapability() {
        Integer result = getContentValues().getAsInteger(Im.CHAT_CAPABILITY);
        return result == null ? 0 : result;
    }

    public String getTitleForDisplay() {
        return ContactsApp.Im_Prot[getProtocol()];
    }

    public String getContentForDisplay() {
        return getData();
    }
}
