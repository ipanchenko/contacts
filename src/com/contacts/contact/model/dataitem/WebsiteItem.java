package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.Website;
import com.contacts.ContactsApp;

public class WebsiteItem extends ContactDataItem {

    public WebsiteItem(ContentValues values) {
        super(values);
    }

    public String getUrl() {
        return getContentValues().getAsString(Website.URL);
    }

    public int getType() {
        return getContentValues().getAsInteger(Website.TYPE);
    }

    public String getLabel() {
        return getContentValues().getAsString(Website.LABEL);
    }

    public String getTitleForDisplay() {
        return getUrl();
    }

    public String getContentForDisplay() {
        return ContactsApp.sTypeNames.get(getMimeType())[getType()];
    }
}
