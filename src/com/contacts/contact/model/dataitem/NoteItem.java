package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.Note;

public class NoteItem extends ContactDataItem {

    public NoteItem(ContentValues values) {
        super(values);
    }

    public String getNote() {
        return getContentValues().getAsString(Note.NOTE);
    }

    public String getTitleForDisplay() {
        //FIXME
        return "Note";
    }

    public String getContentForDisplay() {
        return getNote();
    }
}
