package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import com.contacts.ContactsApp;

public class RelationItem extends ContactDataItem {

    public RelationItem(ContentValues values) {
        super(values);
    }

    public String getName() {
        return getContentValues().getAsString(Relation.NAME);
    }

    public int getType() {
        return getContentValues().getAsInteger(Relation.TYPE);
    }

    public String getLabel() {
        return getContentValues().getAsString(Relation.LABEL);
    }

    public String getTitleForDisplay() {
        return ContactsApp.sTypeNames.get(getMimeType())[getType()];
    }

    public String getContentForDisplay() {
        return getName();
    }
}
