package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.text.TextUtils;
import com.contacts.ContactsApp;

public class StructuredPostalItem extends ContactDataItem {

    public StructuredPostalItem(ContentValues values) {
        super(values);
    }

    public String getFormattedAddress() {
        return getContentValues().getAsString(StructuredPostal.FORMATTED_ADDRESS);
    }

    public int getType() {
        return getContentValues().getAsInteger(StructuredPostal.TYPE);
    }

    public String getLabel() {
        return getContentValues().getAsString(StructuredPostal.LABEL);
    }

    public String getStreet() {
        return getContentValues().getAsString(StructuredPostal.STREET);
    }

    public String getPOBox() {
        return getContentValues().getAsString(StructuredPostal.POBOX);
    }

    public String getNeighborhood() {
        return getContentValues().getAsString(StructuredPostal.NEIGHBORHOOD);
    }

    public String getCity() {
        return getContentValues().getAsString(StructuredPostal.CITY);
    }

    public String getRegion() {
        return getContentValues().getAsString(StructuredPostal.REGION);
    }

    public String getPostcode() {
        return getContentValues().getAsString(StructuredPostal.POSTCODE);
    }

    public String getCountry() {
        return getContentValues().getAsString(StructuredPostal.COUNTRY);
    }

    public String getTitleForDisplay() {
        return ContactsApp.sTypeNames.get(getMimeType())[getType()];
    }

    public String getContentForDisplay() {
        StringBuilder sb = new StringBuilder();
        addLine(sb, getCountry());
        addLine(sb, getCity());
        addLine(sb, getStreet());
        return sb.toString();
    }

    private StringBuilder addLine(StringBuilder sb, String line) {
        if(!TextUtils.isEmpty(line)) {
            sb.append(line);
            sb.append("\n");
        }
        return sb;
    }
}
