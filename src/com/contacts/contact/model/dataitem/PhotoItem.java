package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.Contacts.Photo;

public class PhotoItem extends ContactDataItem {

    public PhotoItem(ContentValues values) {
        super(values);
    }

    public Long getPhotoFileId() {
        return getContentValues().getAsLong(Photo.PHOTO_FILE_ID);
    }

    public byte[] getPhoto() {
        return getContentValues().getAsByteArray(Photo.PHOTO);
    }
}
