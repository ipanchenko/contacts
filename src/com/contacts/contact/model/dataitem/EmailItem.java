package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.Email;
import com.contacts.ContactsApp;

public class EmailItem extends ContactDataItem {

    public EmailItem(ContentValues values) {
        super(values);
    }

    public String getAddress() {
        return getContentValues().getAsString(Email.ADDRESS);
    }

    public String getDisplayName() {
        return getContentValues().getAsString(Email.DISPLAY_NAME);
    }

    public String getData() {
        return getContentValues().getAsString(Email.DATA);
    }

    public int getType() {
        return getContentValues().getAsInteger(Email.TYPE);
    }

    public String getLabel() {
        return getContentValues().getAsString(Email.LABEL);
    }

    public String getTitleForDisplay() {
        return getAddress();
    }

    public String getContentForDisplay() {
        return ContactsApp.sTypeNames.get(getMimeType())[getType()];
    }

}
