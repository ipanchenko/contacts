package com.contacts.contact.model.dataitem;

import android.content.ContentValues;
import android.provider.ContactsContract.CommonDataKinds.Nickname;

public class NicknameItem extends ContactDataItem {

    public NicknameItem(ContentValues values) {
        super(values);
    }

    public String getName() {
        return getContentValues().getAsString(Nickname.NAME);
    }

    public String getLabel() {
        return getContentValues().getAsString(Nickname.LABEL);
    }

}
