package com.contacts.contact.utils;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;

import java.util.ArrayList;

public class ContactWriter {

    private ContentResolver mContentResolver;

    private Uri RAW_URI = RawContacts.CONTENT_URI;

    public ContactWriter(ContentResolver cr) {
        this.mContentResolver = cr;
    }

    public void deleteContact(long contact_id) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        String[] args = new String[] {String.valueOf(contact_id)};

        ops.add(ContentProviderOperation.newDelete(RAW_URI)
                .withSelection(RawContacts.CONTACT_ID + "=?", args)
                .build());
        try {
            mContentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
