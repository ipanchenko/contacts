package com.contacts.contact.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import com.contacts.contact.model.Contact;
import com.contacts.contact.model.RawContact;

import java.util.ArrayList;

public class ContactsReader {

    private ContentResolver mContentResolver;

    private static class ContactQuery {
        static final String[] COLUMNS = new String[] {
                ContactsContract.Contacts.DISPLAY_NAME_SOURCE,
                ContactsContract.Contacts.LOOKUP_KEY,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.DISPLAY_NAME_ALTERNATIVE,
                ContactsContract.Contacts.PHONETIC_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.Contacts.STARRED,
                ContactsContract.Contacts.CONTACT_PRESENCE,
                ContactsContract.Contacts.CONTACT_STATUS,
                ContactsContract.Contacts.CONTACT_STATUS_TIMESTAMP,
                ContactsContract.Contacts.CONTACT_STATUS_RES_PACKAGE,
                ContactsContract.Contacts.CONTACT_STATUS_LABEL,
                ContactsContract.Contacts.Entity.CONTACT_ID,
                ContactsContract.Contacts.Entity.RAW_CONTACT_ID,

                ContactsContract.RawContacts.ACCOUNT_NAME,
                ContactsContract.RawContacts.ACCOUNT_TYPE,
                ContactsContract.RawContacts.DATA_SET,
                ContactsContract.RawContacts.DIRTY,
                ContactsContract.RawContacts.VERSION,
                ContactsContract.RawContacts.SOURCE_ID,
                ContactsContract.RawContacts.SYNC1,
                ContactsContract.RawContacts.SYNC2,
                ContactsContract.RawContacts.SYNC3,
                ContactsContract.RawContacts.SYNC4,
                ContactsContract.RawContacts.DELETED,

                ContactsContract.Contacts.Entity.DATA_ID,
                Data.DATA1,
                Data.DATA2,
                Data.DATA3,
                Data.DATA4,
                Data.DATA5,
                Data.DATA6,
                Data.DATA7,
                Data.DATA8,
                Data.DATA9,
                Data.DATA10,
                Data.DATA11,
                Data.DATA12,
                Data.DATA13,
                Data.DATA14,
                Data.DATA15,
                Data.SYNC1,
                Data.SYNC2,
                Data.SYNC3,
                Data.SYNC4,
                Data.DATA_VERSION,
                Data.IS_PRIMARY,
                Data.IS_SUPER_PRIMARY,
                Data.MIMETYPE,

                ContactsContract.CommonDataKinds.GroupMembership.GROUP_SOURCE_ID,

                Data.PRESENCE,
                Data.CHAT_CAPABILITY,
                Data.STATUS,
                Data.STATUS_RES_PACKAGE,
                Data.STATUS_ICON,
                Data.STATUS_LABEL,
                Data.STATUS_TIMESTAMP,

                ContactsContract.Contacts.PHOTO_URI,
                ContactsContract.Contacts.SEND_TO_VOICEMAIL,
                ContactsContract.Contacts.CUSTOM_RINGTONE,
                ContactsContract.Contacts.IS_USER_PROFILE,
        };

        public static final int DISPLAY_NAME_SOURCE = 0;
        public static final int LOOKUP_KEY = 1;
        public static final int DISPLAY_NAME = 2;
        public static final int ALT_DISPLAY_NAME = 3;
        public static final int PHONETIC_NAME = 4;
        public static final int PHOTO_ID = 5;
        public static final int STARRED = 6;
        public static final int CONTACT_PRESENCE = 7;
        public static final int CONTACT_STATUS = 8;
        public static final int CONTACT_STATUS_TIMESTAMP = 9;
        public static final int CONTACT_STATUS_RES_PACKAGE = 10;
        public static final int CONTACT_STATUS_LABEL = 11;
        public static final int CONTACT_ID = 12;
        public static final int RAW_CONTACT_ID = 13;

        public static final int ACCOUNT_NAME = 14;
        public static final int ACCOUNT_TYPE = 15;
        public static final int DATA_SET = 16;
        public static final int DIRTY = 17;
        public static final int VERSION = 18;
        public static final int SOURCE_ID = 19;
        public static final int SYNC1 = 20;
        public static final int SYNC2 = 21;
        public static final int SYNC3 = 22;
        public static final int SYNC4 = 23;
        public static final int DELETED = 24;

        public static final int DATA_ID = 25;
        public static final int DATA1 = 26;
        public static final int DATA2 = 27;
        public static final int DATA3 = 28;
        public static final int DATA4 = 29;
        public static final int DATA5 = 30;
        public static final int DATA6 = 31;
        public static final int DATA7 = 32;
        public static final int DATA8 = 33;
        public static final int DATA9 = 34;
        public static final int DATA10 = 35;
        public static final int DATA11 = 36;
        public static final int DATA12 = 37;
        public static final int DATA13 = 38;
        public static final int DATA14 = 39;
        public static final int DATA15 = 40;
        public static final int DATA_SYNC1 = 41;
        public static final int DATA_SYNC2 = 42;
        public static final int DATA_SYNC3 = 43;
        public static final int DATA_SYNC4 = 44;
        public static final int DATA_VERSION = 45;
        public static final int IS_PRIMARY = 46;
        public static final int IS_SUPERPRIMARY = 47;
        public static final int MIMETYPE = 48;

        public static final int GROUP_SOURCE_ID = 49;

        public static final int PRESENCE = 50;
        public static final int CHAT_CAPABILITY = 51;
        public static final int STATUS = 52;
        public static final int STATUS_RES_PACKAGE = 53;
        public static final int STATUS_ICON = 54;
        public static final int STATUS_LABEL = 55;
        public static final int STATUS_TIMESTAMP = 56;

        public static final int PHOTO_URI = 57;
        public static final int SEND_TO_VOICEMAIL = 58;
        public static final int CUSTOM_RINGTONE = 59;
        public static final int IS_USER_PROFILE = 60;
    }

    public ContactsReader(ContentResolver cr) {
        this.mContentResolver = cr;
    }

    public Contact getContact(Uri uri){
        Uri entityUri = Uri.withAppendedPath(uri, ContactsContract.Contacts.Entity.CONTENT_DIRECTORY);
        Cursor cursor = mContentResolver.query(entityUri, ContactQuery.COLUMNS, null, null,
                ContactsContract.Contacts.Entity.RAW_CONTACT_ID);
        if (!cursor.moveToFirst()) {
            cursor.close();
        }
        Contact result = loadContactHeaderData(cursor, entityUri);

        long currentRawContactId = -1;
        RawContact rawContact = null;
        ArrayList<RawContact> rawContacts = new ArrayList<RawContact>();
        do {
            long rawContactId = cursor.getLong(ContactQuery.RAW_CONTACT_ID);
            if (rawContactId != currentRawContactId) {
                currentRawContactId = rawContactId;
                rawContact = new RawContact(loadRawContactValues(cursor));
                rawContacts.add(rawContact);
            }
            if (!cursor.isNull(ContactQuery.DATA_ID)) {
                ContentValues data = loadDataValues(cursor);
                rawContact.addDataItemValues(data);
            }
        } while (cursor.moveToNext());
        result.setRawContacts(rawContacts);
        return result;
    }

    private ContentValues loadRawContactValues(Cursor cursor) {
        ContentValues cv = new ContentValues();

        cv.put(ContactsContract.RawContacts._ID, cursor.getLong(ContactQuery.RAW_CONTACT_ID));

        cursorColumnToContentValues(cursor, cv, ContactQuery.ACCOUNT_NAME);
        cursorColumnToContentValues(cursor, cv, ContactQuery.ACCOUNT_TYPE);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA_SET);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DIRTY);
        cursorColumnToContentValues(cursor, cv, ContactQuery.VERSION);
        cursorColumnToContentValues(cursor, cv, ContactQuery.SOURCE_ID);
        cursorColumnToContentValues(cursor, cv, ContactQuery.SYNC1);
        cursorColumnToContentValues(cursor, cv, ContactQuery.SYNC2);
        cursorColumnToContentValues(cursor, cv, ContactQuery.SYNC3);
        cursorColumnToContentValues(cursor, cv, ContactQuery.SYNC4);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DELETED);
        cursorColumnToContentValues(cursor, cv, ContactQuery.CONTACT_ID);
        cursorColumnToContentValues(cursor, cv, ContactQuery.STARRED);

        return cv;
    }

    private Contact loadContactHeaderData(final Cursor cursor, Uri contactUri) {
        final String directoryParameter = contactUri.getQueryParameter(ContactsContract.DIRECTORY_PARAM_KEY);
        final long directoryId = directoryParameter == null
                ? ContactsContract.Directory.DEFAULT
                : Long.parseLong(directoryParameter);
        final long contactId = cursor.getLong(ContactQuery.CONTACT_ID);
        final String lookupKey = cursor.getString(ContactQuery.LOOKUP_KEY);
        final int displayNameSource = cursor.getInt(ContactQuery.DISPLAY_NAME_SOURCE);
        final String displayName = cursor.getString(ContactQuery.DISPLAY_NAME);
        final String altDisplayName = cursor.getString(ContactQuery.ALT_DISPLAY_NAME);
        final String phoneticName = cursor.getString(ContactQuery.PHONETIC_NAME);
        final long photoId = cursor.getLong(ContactQuery.PHOTO_ID);
        final String photoUri = cursor.getString(ContactQuery.PHOTO_URI);
        final boolean starred = cursor.getInt(ContactQuery.STARRED) != 0;
        final Integer presence = cursor.isNull(ContactQuery.CONTACT_PRESENCE)
                ? null
                : cursor.getInt(ContactQuery.CONTACT_PRESENCE);
        final boolean sendToVoicemail = cursor.getInt(ContactQuery.SEND_TO_VOICEMAIL) == 1;
        final String customRingtone = cursor.getString(ContactQuery.CUSTOM_RINGTONE);
        final boolean isUserProfile = cursor.getInt(ContactQuery.IS_USER_PROFILE) == 1;

        Uri lookupUri;
        if (directoryId == ContactsContract.Directory.DEFAULT || directoryId == ContactsContract.Directory.LOCAL_INVISIBLE) {
            lookupUri = ContentUris.withAppendedId(
                    Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey), contactId);
        } else {
            lookupUri = contactUri;
        }

        return new Contact(directoryParameter, directoryId, contactId, lookupKey, displayNameSource,
                displayName, altDisplayName, phoneticName, photoId, photoUri, starred, presence,
                sendToVoicemail, customRingtone, isUserProfile, lookupUri);
    }

    private ContentValues loadDataValues(Cursor cursor) {
        ContentValues cv = new ContentValues();

        cv.put(Data._ID, cursor.getLong(ContactQuery.DATA_ID));

        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA1);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA2);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA3);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA4);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA5);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA6);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA7);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA8);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA9);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA10);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA11);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA12);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA13);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA14);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA15);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA_SYNC1);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA_SYNC2);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA_SYNC3);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA_SYNC4);
        cursorColumnToContentValues(cursor, cv, ContactQuery.DATA_VERSION);
        cursorColumnToContentValues(cursor, cv, ContactQuery.IS_PRIMARY);
        cursorColumnToContentValues(cursor, cv, ContactQuery.IS_SUPERPRIMARY);
        cursorColumnToContentValues(cursor, cv, ContactQuery.MIMETYPE);
        cursorColumnToContentValues(cursor, cv, ContactQuery.GROUP_SOURCE_ID);
        cursorColumnToContentValues(cursor, cv, ContactQuery.CHAT_CAPABILITY);

        return cv;
    }

    private void cursorColumnToContentValues(Cursor cursor, ContentValues values, int index) {
        switch (cursor.getType(index)) {
            case Cursor.FIELD_TYPE_NULL:
                // don't put anything in the content values
                break;
            case Cursor.FIELD_TYPE_INTEGER:
                values.put(ContactQuery.COLUMNS[index], cursor.getLong(index));
                break;
            case Cursor.FIELD_TYPE_STRING:
                values.put(ContactQuery.COLUMNS[index], cursor.getString(index));
                break;
            case Cursor.FIELD_TYPE_BLOB:
                values.put(ContactQuery.COLUMNS[index], cursor.getBlob(index));
                break;
            default:
                throw new IllegalStateException("Invalid or unhandled data type");
        }
    }
}