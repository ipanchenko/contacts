package com.contacts.contact.utils;

import android.content.Context;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.CommonDataKinds.Website;

public class ContactConstants {
	
	private Context mContext;
	
	private static int[] Email_Type = {Email.TYPE_CUSTOM, Email.TYPE_HOME, Email.TYPE_WORK, Email.TYPE_OTHER, Email.TYPE_MOBILE};
	private static String[] Email_Type_Name = {"custom", "home", "work", "other", "mobile"};
	private static int[] Event_Type = {Event.TYPE_CUSTOM, Event.TYPE_ANNIVERSARY,Event.TYPE_OTHER,Event.TYPE_BIRTHDAY};
	private static String[] Event_Type_Name = {"custom", "anniversary","other","birthday"};
	private static int[] Im_Protocol = {Im.PROTOCOL_CUSTOM, Im.PROTOCOL_AIM, Im.PROTOCOL_MSN, Im.PROTOCOL_YAHOO, Im.PROTOCOL_SKYPE, Im.PROTOCOL_QQ,
			Im.PROTOCOL_GOOGLE_TALK, Im.PROTOCOL_ICQ, Im.PROTOCOL_JABBER, Im.PROTOCOL_NETMEETING};
	private static String[] Im_Protocol_Name = {"custom", "aim", "msn", "yahoo", "skype", "qq", "google_talk", "icq", "jabber", "netmeeting"};
	private static int[] Im_Type = {Im.TYPE_CUSTOM, Im.TYPE_HOME, Im.TYPE_WORK, Im.TYPE_OTHER};
	private static String[] Im_Type_Name = {"custom", "home", "work", "other"};
	private static int[] Org_Type = {Organization.TYPE_CUSTOM, Organization.TYPE_WORK, Organization.TYPE_OTHER};
	private static String[] Org_Type_Name = {"custom", "work", "other"};
	private static int[] Phone_Type = {Phone.TYPE_CUSTOM, Phone.TYPE_HOME,Phone.TYPE_MOBILE,Phone.TYPE_WORK, Phone.TYPE_FAX_WORK,
		Phone.TYPE_FAX_HOME, Phone.TYPE_PAGER, Phone.TYPE_OTHER, Phone.TYPE_CALLBACK, Phone.TYPE_CAR, Phone.TYPE_COMPANY_MAIN, Phone.TYPE_ISDN,
		Phone.TYPE_MAIN, Phone.TYPE_OTHER_FAX, Phone.TYPE_RADIO, Phone.TYPE_TELEX, Phone.TYPE_TTY_TDD, Phone.TYPE_WORK_MOBILE, Phone.TYPE_WORK_PAGER,
		Phone.TYPE_ASSISTANT, Phone.TYPE_MMS};
	private static String[] Phone_Type_Name = {"custom", "home", "mobile", "work", "fax_work", "fax_home", "pager", "other", "callback", "car", "company_main", "isdn",
		"main", "other_fax", "radio", "telex", "tty_tdd", "work_mobile", "work_pager", "assistant", "mms"};
	private static int[] Relation_Type = {Relation.TYPE_CUSTOM, Relation.TYPE_ASSISTANT, Relation.TYPE_BROTHER, Relation.TYPE_CHILD, Relation.TYPE_DOMESTIC_PARTNER, Relation.TYPE_FATHER, Relation.TYPE_FRIEND,
			Relation.TYPE_MANAGER, Relation.TYPE_MOTHER, Relation.TYPE_PARENT, Relation.TYPE_PARTNER, Relation.TYPE_REFERRED_BY, Relation.TYPE_RELATIVE, Relation.TYPE_SISTER, Relation.TYPE_SPOUSE};
	private static String[] Relation_Type_Name = {"custom", "assistant", "brother", "child", "domestic_partner", "father", "friend", "manager", "mother", "parent", "partner", "referred_by", "relative", "sister", "spouse"};
	private static int[] Postal_Type = {StructuredPostal.TYPE_CUSTOM, StructuredPostal.TYPE_HOME, StructuredPostal.TYPE_WORK, StructuredPostal.TYPE_OTHER};
	private static String[] Postal_Type_Name = {"custom", "home", "work", "other"};
	private static int[] Website_Type = {Website.TYPE_CUSTOM, Website.TYPE_HOMEPAGE, Website.TYPE_BLOG, Website.TYPE_PROFILE, Website.TYPE_HOME, Website.TYPE_WORK, Website.TYPE_FTP, Website.TYPE_OTHER};
	private static String[] Website_Type_Name = {"custom", "homepage", "blog", "profile", "home", "work", "ftp", "other"};
	
	public ContactConstants(Context context) {
		mContext = context;
	}
	
	public String[] GetEmailsTypes() {
		String[] result = new String[Email_Type.length];
		for(int i=0;i<Email_Type.length;i++) {
			result[i] = mContext.getString(Email.getTypeLabelResource(Email_Type[i]));
		}
		return result;
	}
	
	public String[] GetEventsTypes() {
		String[] result = new String[Event_Type.length];
		for(int i=0;i<Event_Type.length;i++) {
			result[i] = mContext.getString(Event.getTypeResource(Event_Type[i]));
		}
		return result;
	}
	
	public String[] GetImTypes() {
		String[] result = new String[Im_Type.length];
		for(int i=0;i<Im_Type.length;i++) {
			result[i] = mContext.getString(Im.getTypeLabelResource(Im_Type[i]));
		}
		return result;
	}

	public static int GetImTypeNums(String type) {
		for(int i=0;i<Im_Type_Name.length;i++) {
			if(Im_Type_Name[i].compareToIgnoreCase(type)==0)
				return i;
		}
		return Im.TYPE_OTHER;
	}

    public String[] GetImProt() {
        String[] result = new String[Im_Protocol.length];
        for(int i=0;i<Im_Protocol.length;i++) {
            result[i] = mContext.getString(Im.getProtocolLabelResource(Im_Protocol[i]));
        }
        return result;
    }

    public static int GetImProtNums(String type) {
        for(int i=0;i<Im_Protocol_Name.length;i++) {
            if(Im_Protocol_Name[i].compareToIgnoreCase(type)==0)
                return i;
        }
        return Integer.valueOf(Im.CUSTOM_PROTOCOL);
    }
	
	public String[] GetOrgTypes() {
		String[] result = new String[Org_Type.length];
		for(int i=0;i<Org_Type.length;i++) {
			result[i] = mContext.getString(Organization.getTypeLabelResource(Org_Type[i]));
		}
		return result;
	}
	
	public String[] GetPhoneTypes() {
		String[] result = new String[Phone_Type.length];
		for(int i=0;i<Phone_Type.length;i++) {
			result[i] = mContext.getString(Phone.getTypeLabelResource(Phone_Type[i]));
		}
		return result;
	}
	
	public String[] GetRelationTypes() {
//			FIXME: undefined getTypeLabelResource for Relation
		return Relation_Type_Name;
	}
	
	public String[] GetPostalTypes() {
		String[] result = new String[Postal_Type.length];
		for(int i=0;i<Postal_Type.length;i++) {
			result[i] = mContext.getString(StructuredPostal.getTypeLabelResource(Postal_Type[i]));
		}
		return result;
	}
	
	public String[] GetWebsiteTypes() {
		String[] result = new String[Website_Type.length];
//		for(int i=0;i<Website_Type.length;i++) {
//			result[i] = mContext.getString(Website.getTypeLabelResource(Website_Type[i]));
			//FIXME: undefined getTypeLabelResource for Website
//		}
		return result;
	}
	
	public static int GetEmailsTypeNums(String type) {
		for(int i=0;i<Email_Type_Name.length;i++) {
			if(Email_Type_Name[i].compareToIgnoreCase(type)==0)
				return i;
		}
		return Email.TYPE_OTHER;
	}
	
	public static int GetEventsTypeNums(String type) {
		for(int i=0;i<Event_Type_Name.length;i++) {
			if(Event_Type_Name[i].compareToIgnoreCase(type)==0)
				return i;
		}
		return Event.TYPE_OTHER;
	}

	public static int GetOrgTypeNums(String type) {
		for(int i=0;i<Org_Type_Name.length;i++) {
			if(Org_Type_Name[i].compareToIgnoreCase(type)==0)
				return i;
		}
		return Organization.TYPE_OTHER;
	}
	
	public static int GetPhoneTypeNums(String type) {
		for(int i=0;i<Phone_Type_Name.length;i++) {
			if(Phone_Type_Name[i].compareToIgnoreCase(type)==0)
				return i;
		}
		return Phone.TYPE_OTHER;
	}
	
	public static int GetRelationTypeNums(String type) {
		for(int i=0;i<Relation_Type_Name.length;i++) {
			if(Relation_Type_Name[i].compareToIgnoreCase(type)==0)
				return i;
		}
		return Relation.TYPE_RELATIVE;
	}
	
	public static int GetPostalTypeNums(String type) {
		for(int i=0;i<Postal_Type_Name.length;i++) {
			if(Postal_Type_Name[i].compareToIgnoreCase(type)==0)
				return i;
		}
		return StructuredPostal.TYPE_OTHER;
	}

	public static int GetWebsiteTypeNums(String type) {
		for(int i=0;i<Website_Type_Name.length;i++) {
			if(Website_Type_Name[i].compareToIgnoreCase(type)==0)
				return i;
		}
		return Website.TYPE_OTHER;
	}

}
