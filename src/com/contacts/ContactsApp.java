package com.contacts;

import java.io.File;
import java.util.HashMap;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;

import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.provider.ContactsContract.CommonDataKinds.Website;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.provider.ContactsContract.CommonDataKinds.Email;
import com.contacts.contact.utils.ContactConstants;

public class ContactsApp extends Application {

    public static SharedPreferences sPref;
    public static HashMap<String, String[]> sTypeNames = new HashMap<String, String[]>();
    public static String[] Im_Prot;

    private Context mContext = this;

    @Override
    public void onCreate() {
        super.onCreate();
        sPref = getSharedPreferences("settings", MODE_PRIVATE);
        initContactConstants();
    }

    private void initContactConstants() {
        ContactConstants cc = new ContactConstants(mContext);
        sTypeNames.put(Email.CONTENT_ITEM_TYPE, cc.GetEmailsTypes());
        sTypeNames.put(Event.CONTENT_ITEM_TYPE, cc.GetEventsTypes());
        sTypeNames.put(Im.CONTENT_ITEM_TYPE, cc.GetImTypes());
        sTypeNames.put(Organization.CONTENT_ITEM_TYPE, cc.GetOrgTypes());
        sTypeNames.put(Phone.CONTENT_ITEM_TYPE, cc.GetPhoneTypes());
        sTypeNames.put(StructuredPostal.CONTENT_ITEM_TYPE, cc.GetPostalTypes());
        sTypeNames.put(Website.CONTENT_ITEM_TYPE, cc.GetWebsiteTypes());
        sTypeNames.put(Relation.CONTENT_ITEM_TYPE, cc.GetPostalTypes());
        Im_Prot = cc.GetImProt();
    }

}
