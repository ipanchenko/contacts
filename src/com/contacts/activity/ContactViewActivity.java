package com.contacts.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.widget.LinearLayout;
import com.contacts.R;
import com.contacts.contact.model.Contact;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.utils.ContactWriter;
import com.contacts.contact.utils.ContactsReader;
import com.contacts.view.ItemView;
import com.contacts.view.NameView;
import com.contacts.view.ProxyView;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactViewActivity extends Activity {

    private Contact mContact;
    private LinearLayout lLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_view);
        Uri uri = getIntent().getData();
        if(uri != null) {
            ContactsReader cr = new ContactsReader(getContentResolver());
            mContact = cr.getContact(uri);
        }
        if(mContact == null)
            finish();
        getContactLayout();
    }

    private void getContactLayout() {
        lLayout = (LinearLayout) findViewById(R.id.contact_view);
        lLayout.removeAllViews();

        HashMap<String, ArrayList<ContentValues>> map = mContact.getValues().getMap();
        addNameView();
        addHeaders(map);
    }

    private void addNameView() {
        NameView name = new NameView();
        lLayout.addView(name.getView(mContact, this));
    }

    private void addHeaders(HashMap<String, ArrayList<ContentValues>> map) {
        for(ArrayList<ContentValues> items : map.values())
            for(int i = 0; i < items.size(); i++)
                addField(ContactDataItem.createFrom(items.get(i)));
    }

    private void addField(ContactDataItem item) {
        ItemView view = ProxyView.getViewClass(item);
        if(view != null)
            lLayout.addView(view.getView(item, this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, R.id.menu_edit, 0, getString(R.string.edit))
                .setIcon(R.drawable.ic_menu_edit)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        menu.add(Menu.NONE, R.id.menu_delete, 0, getString(R.string.delete))
                .setIcon(R.drawable.ic_menu_delete)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menu_edit) {
            Intent edit = new Intent(this, ContactEditActivity.class);
            edit.setData(mContact.getLookupUri());
            startActivityForResult(edit, 1);
            return true;
        }
        if (item.getItemId()==R.id.menu_delete) {
            deleteDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteDialog() {
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle("Delete ");
        ad.setMessage("Do you want to delete contact?");
        ad.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                ContactWriter cw = new ContactWriter(getContentResolver());
                cw.deleteContact(Integer.valueOf(mContact.getLookupUri().getLastPathSegment()));
            }
        });
        ad.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
            }
        });
        ad.setCancelable(true);
        ad.show();
    }

}