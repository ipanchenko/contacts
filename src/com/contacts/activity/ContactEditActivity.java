package com.contacts.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.*;
import android.view.*;
import android.widget.*;

import com.contacts.R;
import com.contacts.contact.model.Contact;
import com.contacts.contact.model.dataitem.ContactDataItem;
import com.contacts.contact.utils.ContactsReader;
import com.contacts.view.ItemView;
import com.contacts.view.edit.NameView;
import com.contacts.view.edit.ProxyView;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactEditActivity extends Activity {

    private HashMap<String, LinearLayout> mHeaders= new HashMap<String, LinearLayout>();
    private ArrayList<ItemView> itemViews = new ArrayList<ItemView>();
    private LinearLayout mLayout;
    private LayoutInflater mInflater;
    private Contact mContact;
    private HashMap<String, ArrayList<ContentValues>> mDataItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_edit_view);

        mLayout = (LinearLayout) findViewById(R.id.contact_edit_layout);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //FIXME Contact no longer Parcelable and getLastNonConfigurationInstance now are deprecated
        //FIXME and Contact does not losing due to orientation changes so I don`t need this code here anymore
        //FIXME but what if Contact can be lost by some other way so I need fix it somehow
        //Contact contact_ = (Contact) getLastNonConfigurationInstance();
//        if(contact_ != null)
//            mContact = contact_;
//        else

        mContact = loadContact();

        inflateViews();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void inflateViews() {
        mDataItems = mContact.getValues().getMap();
        addNameView();
        addHeader(Nickname.CONTENT_ITEM_TYPE, false);
        addHeader(Email.CONTENT_ITEM_TYPE, true);
        addHeader(Phone.CONTENT_ITEM_TYPE, true);
        addHeader(Im.CONTENT_ITEM_TYPE, true);
        addHeader(Website.CONTENT_ITEM_TYPE, true);
        addHeader(StructuredPostal.CONTENT_ITEM_TYPE, true);
        addHeader(Organization.CONTENT_ITEM_TYPE, true);
        addHeader(Event.CONTENT_ITEM_TYPE, true);
        addHeader(Relation.CONTENT_ITEM_TYPE, true);
        addHeader(Note.CONTENT_ITEM_TYPE, false);
    }

    private Contact loadContact() {
        Uri uri = getIntent().getData();
        ContactsReader reader = new ContactsReader(getContentResolver());
        //FIXME create or edit existing contact
        if (uri != null)
            return reader.getContact(uri);
        this.finish();
        return null;
    }

    //save changes
    private void saveContact() {

    }

//    private void readContactFields() {
//        Contact contact = new Contact();
//        mNameView.getValue(contact);
//        for(int i = 0; i < itemViews.size(); i++) {
//            ContactDataItem item = itemViews.get(i).getValue();
//            if(!contact.rawContacts.containsKey(item.getMimeType()))
//                contact.rawContacts.put(item.getMimeType(), new ArrayList<ContactDataItem>());
//            ArrayList<ContactDataItem> modelItems = contact.rawContacts.get(item.getMimeType());
//            modelItems.add(item);
//        }
//        diffContact = new Pair<Contact, Contact>(mContact, contact);
//    }

    //edit interface and add new fields
    private void addHeader(final String mime, boolean isAddible) {
        mHeaders.put(mime, new LinearLayout(this));
        mHeaders.get(mime).setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        mLayout.addView(mHeaders.get(mime), llp);
        mHeaders.get(mime).addView(mInflater.inflate(R.layout.add_field, null));

        TextView field_title = (TextView)mHeaders.get(mime).findViewById(R.id.field_title);
        field_title.setText(ProxyView.getMimeName(mime));
        ImageView field_add = (ImageView)mHeaders.get(mime).findViewById(R.id.field_add);

        if(isAddible) {
            field_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addField(mime);
                }
            });
        } else {
            field_add.setVisibility(View.GONE);
            if(!mDataItems.containsKey(mime) || mDataItems.get(mime).size() == 0) {
                addField(mime);
            }
        }

        if(mDataItems.containsKey(mime))
            for(int i = 0; i < mDataItems.get(mime).size(); i++)
                addField(mDataItems.get(mime).get(i));

    }

    private void addField(String mime) {
        if(!mDataItems.containsKey(mime))
            mDataItems.put(mime, new ArrayList<ContentValues>());

        ContentValues val = new ContentValues();
        val.put(ContactsContract.Contacts.Data.MIMETYPE, mime);
        mDataItems.get(mime).add(val);
        addField(val);
    }

    private void addField(ContentValues val) {
        ContactDataItem item = new ContactDataItem(val);
        ItemView view = ProxyView.getViewClass(item);
        itemViews.add(view);
        mHeaders.get(item.getMimeType()).addView(view.getView(item, this));
    }

    private void addNameView() {
        NameView mNameView = new NameView();
        mLayout.addView(mNameView.getView(mContact, this));
    }

    //menu
    private void cancelEdit() {
        AlertDialog.Builder ad;
        ad = new AlertDialog.Builder(ContactEditActivity.this);
        ad.setTitle("Cancel");
        ad.setMessage("Do you want to discard all changes?");
        ad.setPositiveButton("Yes", new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                ContactEditActivity.this.finish();
            }
        });
        ad.setNegativeButton("No", new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
            }
        });
        ad.setCancelable(true);
        ad.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            cancelEdit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        //FIXME
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //FIXME i18n
        menu.add(Menu.NONE, R.id.menu_save, 0, R.string.edit_save)
                .setIcon(R.drawable.ic_menu_save)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(Menu.NONE, R.id.menu_cancel, 0, R.string.edit_cancel)
                .setIcon(R.drawable.ic_menu_close_clear_cancel)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menu_save) {
            saveContact();
            return true;
        }
        if (item.getItemId()==R.id.menu_cancel) {
            cancelEdit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
