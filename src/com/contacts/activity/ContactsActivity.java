package com.contacts.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import com.contacts.fragments.ContactsListFragment;

public class ContactsActivity extends FragmentActivity implements ContactsListFragment.OnContactsInteractionListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            ContactsListFragment lf = new ContactsListFragment();
            ft.replace(android.R.id.content, lf);
        } else {
            ContactsListFragment lf = new ContactsListFragment();
            ft.replace(android.R.id.content, lf);
        }
        ft.commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String searchQuery = intent.getStringExtra(SearchManager.USER_QUERY);
            ContactsListFragment fragment = (ContactsListFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
            fragment.setSearchQuery(searchQuery);
        }
        super.onNewIntent(intent);
    }

    @Override
    public void onContactSelected(Uri contactUri) {
        Intent intent = new Intent(this, ContactViewActivity.class);
        intent.setData(contactUri);
        startActivity(intent);
    }

    @Override
    public void onSelectionCleared() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        return super.onKeyDown(keyCode, event);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ContactsListFragment fragment = (ContactsListFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}
