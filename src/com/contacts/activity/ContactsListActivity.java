package com.contacts.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import com.contacts.R;
import com.contacts.fragments.ContactsListFragment;

public class ContactsListActivity extends FragmentActivity implements ContactsListFragment.OnContactsInteractionListener {

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_list_activity);

        ContactsListFragment listFragment = (ContactsListFragment) getSupportFragmentManager().findFragmentById(R.id.contact_list);
		
		Intent intent = getIntent();
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String searchQuery = getIntent().getStringExtra(SearchManager.QUERY);			
			listFragment.setSearchQuery(searchQuery);
		}
	}

	@Override
	public void onContactSelected(Uri contactUri) {
		Intent intent = new Intent(this, ContactViewActivity.class);
		intent.setData(contactUri);
		startActivity(intent);
	}
	

	@Override
	public void onSelectionCleared() {

	}
	

	@Override
	public boolean onSearchRequested() {
		return super.onSearchRequested();
	}
}
